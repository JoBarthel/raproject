﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;

public class Pole : MonoBehaviour
{
    //this lets us track how many move where made
    public MoveNumberTracker moveTracker; 
    
    //those are children of the Pole object
    //these prefabs are used to display the ring once they're on the pole
    //TODO : animate it
    public RingModel Ring0;
    public RingModel Ring1;
    public RingModel Ring2;

    public bool startingPole = false;
    public bool arrivalPole;
    private bool firstTimeDetected = true;
    private List<RingModel> ringModels;

    //where do our ring start falling from when they added ?
    public float topYcoordinate;
    
    // this stacks hold the rings currently attached to the pole
    private Stack<Ring> rings;


    private void Awake()
    {
        ringModels = new List<RingModel> {Ring0, Ring1, Ring2};
        rings = new Stack<Ring>();
        //the starting pole starts with a full stack
        if (startingPole)
        {
            rings.Push(new Ring(2));
            rings.Push(new Ring(1));
            rings.Push(new Ring(0));
            startingPole = false;
        }

        topYcoordinate = transform.position.y + transform.localScale.y/2;
        
    }

    private void OnEnable()
    {

        UpdateRingDisplay();
    }


    // try to add [newRing] to the pole
    // you can only add a ring if the pole is empty or if the ring at top of the stack
    //is bigger than newRing
    public bool AddRing(Ring newRing)
    {
        if (rings.Count == 0)
        {
            rings.Push(newRing);
            UpdateRingDisplay();
            moveTracker.Increase();
            return true;
        }

        if (rings.Peek().Size <= newRing.Size)
        {
            StartCoroutine(PulseColor(.3f, Color.red, 1));
            return false;
        }
        
        rings.Push(newRing);
        UpdateRingDisplay();
        moveTracker.Increase();
        
        if(arrivalPole)
            CheckForVictory();
        
        return true;



    }
    
    //try to put the topmost ring in ring
    //if its not possible, return false
    public bool RemoveRing(out Ring ring)
    {
        if (rings.Count == 0)
        {
            ring = null;
            return false;
        }

        ring = rings.Pop();
        UpdateRingDisplay();
        return true;
    }

    private void UpdateRingDisplay()
    {
        
        foreach (RingModel ringModel in ringModels)
        {
            ringModel.GetComponent<Renderer>().enabled = false;
        }

        foreach (Ring ring in rings)
        {
            RingModel currentModel = ringModels[2 - ring.Size];
            if (currentModel.enabled == false) ;
            Debug.Log("enabling component " + (2 - ring.Size) );
            ringModels[ 2 - ring.Size ].GetComponent<Renderer>().enabled = true;

        }
    }


    private bool HasRingOfSize(int size)
    {
        foreach (Ring ring in rings)
        {
            if (ring.Size == size)
                return true;
        }

        return false;
    }

    private void CheckForVictory()
    {
        if (rings.Count == 3 && arrivalPole)
            Win();
    }

    private void Win()
    {
        AudioSource audioSource = GetComponent<AudioSource>();
        if(audioSource != null)
            audioSource.Play();
        StopAllCoroutines();
        StartCoroutine(PulseColor(.5f, Color.green, 5));
    }

    IEnumerator PulseColor(float animationTime, Color color, int loopNumber)
    {
        Renderer renderer = GetComponent<Renderer>();
        if (renderer != null)
        {
            for (int loop = 0; loop < loopNumber; loop++)
            {
                Color baseColor = renderer.material.color;
                float currentTime = 0;
                while (currentTime < animationTime)
                {
                    currentTime += Time.deltaTime;
                    float fracTime = currentTime / animationTime;
                    float red = Mathf.Lerp(baseColor.r, color.r, fracTime);
                    float blue = Mathf.Lerp(baseColor.b, color.b, fracTime);
                    float green = Mathf.Lerp(baseColor.g, color.g, fracTime);
                    renderer.material.color = new Color(red, green, blue);
                    yield return null;
                }

                renderer.material.color = Color.white;
            }
        }
    }
}
