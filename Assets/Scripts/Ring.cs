
    using UnityEngine;

public class Ring
    {
        //Rings exist in 3 sizes { 0, 1, 2}
        //A Ring can only be put above a ring of bigger size
        public int Size { get; private set; }
        
        //the color depends on the size
        public Color RingColor { get; private set; }
        public Ring(int size)
        {
            Size = size;
            switch (size)
            {
                    case 2 :
                        RingColor = Color.red;
                        break;
                    case 1:
                        RingColor = Color.green;
                        break;
                    case 0:
                        RingColor = Color.blue;
                        break;
                    default:
                        RingColor = Color.white;
                        break;
                            
            }
        }
    }
