using System.Collections;
using System.Runtime.CompilerServices;
using UnityEditor;
using UnityEngine;

    public class RingModel : MonoBehaviour
    {
        private Ring ring;

        // Rings exist in 3 sizes
        // 2 is big red
        // 1 is medium green
        // 0 is small blue
        
        public int size;
        //how much is each rings spaced out ?  
        public float movementUnit = .327f;

        //how long is out falling animùation ? 
        public float fallAnimationTime = .5f;
        
        public void SetVisible(bool status, Color color)
        {
            Renderer renderer = GetComponent<Renderer>();
            if(renderer == null)
                return;

            Color newColor = color;
            newColor.a = status ? 1 : 0;
            renderer.material.color = newColor;

        }

        //play animation of falling down the pole
        //how far we fall depends of how many disks are already on the pole [order] 
        public void Animate(int order, float startingY)
        {
            //our current y pos is at the top of the pole
            //we want to fall down to order * movementUnit
            float endY = transform.position.y - order * movementUnit;
            //start animation
            StartCoroutine( FallAnimation(startingY, endY, fallAnimationTime));
        }
        
        //generic falling animation
        private IEnumerator FallAnimation(float startY, float endY, float animationTime)
        {
            float currentTime = 0;
            while (currentTime < animationTime)
            {
                currentTime += Time.deltaTime;
                float fracTime = currentTime / animationTime;
                float currentY = Mathf.Lerp(startY, endY, fracTime);
                transform.position = new Vector3(transform.position.x, currentY, transform.position.z);
                yield return null;
            }
        }
    }