﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighlightOnMouseOver : MonoBehaviour
{

    private Color baseColor;
    private new Renderer renderer;
    private void Start()
    {
        renderer = GetComponentInChildren<Renderer>();

        baseColor = renderer.material.color;
    }

    private void OnMouseOver()
    {
        Debug.Log("mouseover");
        if(renderer != null)
            renderer.material.color = Color.red;
    }

    private void OnMouseExit()
    {
        renderer.material.color = baseColor;
    }

    private void OnMouseDrag()
    {
        Vector3 mousePos = MyArCamera.ARCamera.ScreenToWorldPoint(Input.mousePosition);
        transform.position = mousePos;
        Debug.Log("dragging");
    }
}
