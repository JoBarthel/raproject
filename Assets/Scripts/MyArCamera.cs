﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyArCamera : MonoBehaviour
{
    public static Camera ARCamera;
    // Start is called before the first frame update
    void Start()
    {
        ARCamera = GetComponent<Camera>();
        if(ARCamera == null)
            Debug.LogWarning("Attention, pas de caméra AR trouvée");
    }
}
