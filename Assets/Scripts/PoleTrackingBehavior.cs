﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using UnityEngine;
using UnityEngine.UI;

public class PoleTrackingBehavior : DefaultTrackableEventHandler
{
    public Text warningText;
    protected override void OnTrackingFound()
    {
        var rendererComponents = GetComponentsInChildren<Renderer>(true);
        var colliderComponents = GetComponentsInChildren<Collider>(true);
        var canvasComponents = GetComponentsInChildren<Canvas>(true);
        var poleComponents = GetComponentsInChildren<Pole>(true);
        // Enable rendering:
        foreach (var component in rendererComponents)
        {
            if(!component.CompareTag("Ring"))
                component.enabled = true;
        }

        // Enable colliders:
        foreach (var component in colliderComponents)
            component.enabled = true;

        // Enable canvas':
        foreach (var component in canvasComponents)
            component.enabled = true;

        foreach (Pole pole in poleComponents)
        {
            pole.enabled = true;
        }
        warningText.enabled = false;
       
    }
    
    protected override void OnTrackingLost()
    {
        var rendererComponents = GetComponentsInChildren<Renderer>(true);
        var colliderComponents = GetComponentsInChildren<Collider>(true);
        var canvasComponents = GetComponentsInChildren<Canvas>(true);
        var poleComponents = GetComponentsInChildren<Pole>(true);
        // Disable rendering:
        foreach (var component in rendererComponents)
            component.enabled = false;

        // Disable colliders:
        foreach (var component in colliderComponents)
            component.enabled = false;

        // Disable canvas':
        foreach (var component in canvasComponents)
            component.enabled = false;
        
        foreach (Pole pole in poleComponents)
        {
            pole.enabled = false;
        }
        
        warningText.enabled = true;
        warningText.text = "Tracking Lost. Remettez la carte \"Plateau de jeu\" en face de la caméra";
    }
}
