﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RingHolder : MonoBehaviour
{
    private Ring ring;
    public RingModel model;

    private bool gracePeriodOver = true;
    private float gracePeriodTimeSpent = .0f;
    public float gracePeriod = .5f;


    //handle playing sounds when rings are moved around
    private AudioSource audioSource;
    public AudioClip removeRingSound;
    public AudioClip addRingSound;
    public AudioClip wrongSound;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (gracePeriodOver) return;
        
        gracePeriodTimeSpent += Time.deltaTime;
        if (gracePeriodTimeSpent >= gracePeriod)
        {
            gracePeriodOver = true;
            gracePeriodTimeSpent = .0f;
        }
    }

    private bool HasRing()
    {
        return ring != null;
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if(!gracePeriodOver)
            return;
        
        Debug.Log("cllision");
        Pole pole = other.GetComponent<Pole>();
        if(pole == null )
            return;
            
        //si on porte déjà un ring
        if (HasRing())
        {
            //on essaie de le poser
            if(pole.AddRing(ring))
                RemoveRing();
            else
                audioSource.PlayOneShot(wrongSound);                
                
            return;
        }
            

        //sinon
        //on ramasse le ring du haut
        if (pole.RemoveRing(out ring))
            AddRing();

    }
        
    private void RemoveRing()
    {
        Debug.Log("Removed Ring");
        ring = null;
        audioSource.PlayOneShot(removeRingSound);
        model.SetVisible(false, Color.white);
    }
        
    private void AddRing()
    {

        Debug.Log("Added Ring");
        audioSource.PlayOneShot(addRingSound);
        model.SetVisible(true, ring.RingColor);

    }
}
