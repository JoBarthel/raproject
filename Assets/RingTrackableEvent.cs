﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class RingTrackableEvent : DefaultTrackableEventHandler
{
    public Text warningText;

    protected override void OnTrackingLost()
    {
        base.OnTrackingLost();
        warningText.enabled = true;
        warningText.text = "Tracking Lost. Remettez la carte \"Cercle rouge\" en face de la caméra";
    }

    protected override void OnTrackingFound()
    {
        base.OnTrackingFound();
        warningText.text = "Tracking Lost. Remettez la carte \"Cercle rouge\" en face de la caméra";
        warningText.enabled = false;
        if (m_NewStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
            warningText.enabled = true;
    }
}
