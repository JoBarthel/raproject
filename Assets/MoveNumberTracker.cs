﻿using UnityEngine;
using UnityEngine.UI;

public class MoveNumberTracker : MonoBehaviour
{
    private int moveNumber = 0;

    private Text text;
    private void Start()
    {
        text = GetComponent<Text>();
        text.text = moveNumber.ToString();
    }

    public void Increase()
    {
        moveNumber++;
        text.text = moveNumber.ToString();
    }
}
