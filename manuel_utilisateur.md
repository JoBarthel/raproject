# Mise en place

## 1. Imprimer et découper les deux images du fichier "a_imprimer", en prenant soin de laisser une marge autour qui permettre de manipuler les images.
Utiliser du papier plus épais permet de meilleurs résultats.
## 2. Placer l'image qui indique "C'est ici que sera placé le plateau de jeu" (nommée carte "Plateau de jeu" çi-après) sur une surface bien plane, telle qu'une table.
## 3. Tenir la caméra à la main, bien haut à la verticale au dessus de l'image "Plateau de jeu". L'image est dans le bon sens si le texte est dans le sens de la lecture. L'entièreté de l'image doit être dans la caméra, et il doit rester suffisemment de place autour pour faire tenir l'autre image. Pour obtenir un angle de vue suffisemment large, nous recommandons de vous tenir debout. 
## 4. Le jeu comportant des feedbacks sonores, nous recommandons d'allumer le son de votre appareil.
## 5. Lancer le jeu. Le plateau de jeu (trois poteaux blancs, dont l'un porte trois disques de couleur) doit s'afficher.  

# Comment jouer

## 1. Se saisir de l'image "Ceci est un cercle rouge que vous pouvez déplacer" (nommée carte "Cercle rouge" çi-après). Attention, cette carte servira à déplacer tout les cercles et pas seulement le rouge. Les raisons de cette erreur sont expliquées dans le rapport. Prendre bien soin de mettre les doigts sur la marge blanche autour de l'image - ils ne doivent pas couvrir la surface imprimée. 
## 2. Placer la surface imprimée face à la caméra. Un cercle blanc doit s'afficher, ce qui signale qu'il est prêt à être utilisé. 
## 3. Pour soulever un anneau hors d'un poteau, amener la carte "Cercke rouge" contre le poteau choisi, en la conservant tounée vers la caméra. Si le transfert est réussi, l'anneau le plus haut est retiré du poteau. Le cercle transparent prend alors la couleur de l'anneau soulevé. 
## 4. Pour déposer un anneau soulevé, amenez la carte "Cercle rouge" contre le poteau choisi. Si le coup est légal, l'anneau apparaît autour du poteau. Le cercle lié à la carte "Cercle rouge" redevient trasnparent. 
## 5. Si le poteau selectionné clignote en rouge, le coup n'est pas légal. Déposez l'anneau ailleurs !

# Troobleshooting 

## Le plateau bouge quand j'essaie d'interagir avec lui !
### Pour de meilleurs résultats, tenez la carte "Cercle rouge" à l'horizontal, en face de la caméra ; puis inclinez la légèrement vers la bas, de façon à ce l'image vous fasse face. Approchez ensuite la carte du poteau avec des mouvements lents, en conservant l'inclinaison. 

## Le plateau ne s'affiche pas ! 
### Le jeu est sensible à la luminosité ambiante. Essayez de le mettre dans un endroit plus éclairé, de préférence en intérieur. 

## Le cercle transparent ne s'affiche pas. 
### Attention à bien laisser une marge autour de l'image des deux cartes. La manipulation doit s'effectuer avec les doigts sur cette marge, sans jamais que l'image ne soit couverte.

## Un anneau est bloqué hors d'un poteau. 
### Remettez la carte "Cercle rouge" bien en face de la caméra. 

## J'ai perdu un anneau/le plateau... 
### Remettez la carte "Plateau de jeu" bien visible, en face de la caméra. Remettez ensuite la carte "Cercle rouge" bien visible, en face de la caméra, sans couvrir "Plateau de jeu". Si cela ne fonctionne pas, redémarrez le jeu. 

## Le jeu  ne fonctionne pas sur mon portable
### L'apk a été testé sur un Galaxy A3 avec Android 6.0.1 . Je n'ai malheureusement pas pu conduire des tests extensifs avec de nombreuses versions d'Android, je vous prie donc de me contacter à josephine.barthel@ensiie.fr si jamais vous avez des problèmes de compatibilité. 